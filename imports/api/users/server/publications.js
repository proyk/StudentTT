import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';

Meteor.publish('Users.single', function(userId) {
  check(userId, String);

  const query = { _id: userId };
  const options = {};

  return Meteor.users.find(query, options);
});

Meteor.publish('Users.inList', function(limit, search) {
  check(search, Match.Maybe(String));
  check(limit, Number);

  const query = {
    roles: { $ne: 'admin' },
  };
  const options = {
    sort: { createdAt: -1 },
    limit,
  };

  if (search) {
    _.extend(query, {
      'profile.name': { $regex: search, $options: 'i' },
    });
  }

  return Meteor.users.find(query, options);
});
