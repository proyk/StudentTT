import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Roles } from 'meteor/alanning:roles';
import Classrooms from '../classrooms/classrooms';

export const newUser = new ValidatedMethod({
  name: 'Users.new',
  validate: new SimpleSchema({
    name: { type: String },
    email: { type: String },
    password: { type: String },
    classrooms: { type: [Object], optional: true, blackbox: true }
  }).validator(),
  run({ email, password, classrooms, name }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    const userId = Accounts.createUser({
      email, password, profile: { name, classrooms },
    });

    if (!this.isSimulation) {
      classrooms.forEach((classroom) => {
        Classrooms.update({ _id: classroom._id }, { $inc: { studentCount: 1 } });
      });

      Roles.addUsersToRoles(userId, ['student']);
    }
  },
});

export const deleteUser = new ValidatedMethod({
  name: 'Users.delete',
  validate: new SimpleSchema({
    userId: { type: String },
    classrooms: { type: [Object], optional: true, blackbox: true }
  }).validator(),
  run({ userId, classrooms }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    Meteor.users.remove(userId);

    if (!this.isSimulation) {
      classrooms.forEach((classroom) => {
        Classrooms.update({ _id: classroom._id }, { $inc: { studentCount: -1 } });
      });
    }

  },
});

export const changeUserEmail = new ValidatedMethod({
  name: 'Users.changeEmail',
  validate: new SimpleSchema({
    userId: { type: String },
    email: { type: String },
  }).validator(),
  run({ userId, email }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    if (!this.isSimulation) {
      if (Meteor.users.findOne({ 'emails.address': email })) {
        throw new Meteor.Error(402, 'Email already exists!');
      }

      const currentUser = Meteor.users.findOne(userId);
      const oldEmail = currentUser.emails[0].address;

      Accounts.removeEmail(userId, oldEmail);
      Accounts.addEmail(userId, email);
    }
  },
});

export const setUserPassword = new ValidatedMethod({
  name: 'Users.setPassword',
  validate: new SimpleSchema({
    userId: { type: String },
    newPassword: { type: String },
  }).validator(),
  run({ userId, newPassword }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    if (!this.isSimulation) {
      Accounts.setPassword(userId, newPassword)
    }
  },
});

export const changeUserName = new ValidatedMethod({
  name: 'Users.changeUserName',
  validate: new SimpleSchema({
    userId: { type: String },
    name: { type: String },
  }).validator(),
  run({ userId, name }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    Meteor.users.update(userId, { $set: { 'profile.name': name } });
  },
});

export const changeUserClassrooms = new ValidatedMethod({
  name: 'Users.changeUserClassrooms',
  validate: new SimpleSchema({
    userId: { type: String },
    classrooms: { type: [Object], optional: true, blackbox: true },
  }).validator(),
  run({ userId, classrooms }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    if (!this.isSimulation) {
      const currentUser = Meteor.users.findOne(userId, { fields: { 'profile.classrooms': 1 } });
      const oldClassrooms = currentUser && currentUser.profile.classrooms || [];

      Meteor.users.update(userId, { $set: { 'profile.classrooms': classrooms } });


      // dec
      oldClassrooms.forEach((classroom) => {
        Classrooms.update({ _id: classroom._id }, { $inc: { studentCount: -1 } });
      });

      // inc
      classrooms.forEach((classroom) => {
        Classrooms.update({ _id: classroom._id }, { $inc: { studentCount: 1 } });
      });
    }
  },
});
