import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import Tasks from './tasks';
import { getUserClassroomIds } from '../../ui/lib/users';

export const submitTask = new ValidatedMethod({
  name: 'Tasks.submit',
  validate: new SimpleSchema({
    taskId: { type: String, optional: true },
    subject: { type: String },
    details: { type: Object, blackbox: true },
    classroom: { type: Object, blackbox: true },
    dueAt: { type: Date },
  }).validator(),
  run({ taskId, subject, details, classroom, dueAt }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    if (taskId) {
      Tasks.update(taskId, {
        $set: { subject, details, classroom, dueAt },
      });
    } else {
      Tasks.insert({ subject, details, classroom, dueAt });
    }
  },
});

export const deleteTask = new ValidatedMethod({
  name: 'Tasks.delete',
  validate: new SimpleSchema({
    taskId: { type: String },
  }).validator(),
  run({ taskId }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    Tasks.remove(taskId);
  },
});

export const completeTask = new ValidatedMethod({
  name: 'Tasks.complete',
  validate: new SimpleSchema({
    taskId: { type: String },
  }).validator(),
  run({ taskId }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    Tasks.update(taskId, { $addToSet: { users: this.userId } });
  },
});

export const getLastTask = new ValidatedMethod({
  name: 'Tasks.getLast',
  validate: null,
  run() {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    const now = new Date();
    const currentUser = Meteor.user();

    let lastTask;

    if (!this.isSimulation) {
      lastTask = Tasks.findOne({
        'classroom._id': { $in: getUserClassroomIds(currentUser) },
        dueAt: { $gte: now }
      }, { sort: { dueAt: 1 } });
    }

    return lastTask;
  },
});

