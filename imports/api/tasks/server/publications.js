import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import Tasks from '../tasks.js';

Meteor.publish('Tasks.single', function(taskId) {
  check(taskId, String);

  const query = { _id: taskId };
  const options = {};

  return Tasks.find(query, options);
});

Meteor.publish('Tasks.inList', function(limit, search) {
  check(search, Match.Maybe(String));
  check(limit, Number);

  const query = {};
  const options = {
    sort: { createdAt: -1 },
    fields: { users: 0, details: 0 },
    limit,
  };

  if (search) {
    _.extend(query, {
      subject: { $regex: search, $options: 'i' },
    });
  }

  return Tasks.find(query, options);
});

Meteor.publish('Tasks.forStudent', function(classroomIds, limit) {
  check(classroomIds, [Match.Maybe(String)]);

  const query = {
    'classroom._id': { $in: classroomIds },
  };
  const options = {
    sort: { createdAt: -1 },
    limit,
  };

  return Tasks.find(query, options);
});
