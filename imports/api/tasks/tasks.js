import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

const Tasks = new Mongo.Collection('Tasks');

Tasks.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Tasks.schema = new SimpleSchema({
  subject: { type: String },
  details: { type: Object, blackbox: true },
  classroom: { type: Object, blackbox: true },
  users: { type: [String], optional: true },
  dueAt: { type: Date },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();
      }
    },
  },
});

Tasks.attachSchema(Tasks.schema);

export default Tasks;
