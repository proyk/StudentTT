import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

const Lessons = new Mongo.Collection('Lessons');

Lessons.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Lessons.schema = new SimpleSchema({
  subject: { type: String },
  teacher: { type: String },
  room: { type: String },
  classroom: { type: Object, blackbox: true },
  dueAt: { type: Date },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();
      }
    },
  },
});

Lessons.attachSchema(Lessons.schema);

export default Lessons;
