import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import Lessons from './lessons';
import { getUserClassroomIds } from '../../ui/lib/users';

export const submitLesson = new ValidatedMethod({
  name: 'Lessons.submit',
  validate: new SimpleSchema({
    lessonId: { type: String, optional: true },
    subject: { type: String },
    teacher: { type: String },
    room: { type: String },
    classroom: { type: Object, blackbox: true },
    dueAt: { type: Date },
  }).validator(),
  run({ lessonId, subject, teacher, room, classroom, dueAt }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    if (lessonId) {
      Lessons.update(lessonId, {
        $set: { subject, teacher, room, classroom, dueAt },
      });
    } else {
      Lessons.insert({ subject, teacher, room, classroom, dueAt });
    }
  },
});

export const deleteLesson = new ValidatedMethod({
  name: 'Lessons.delete',
  validate: new SimpleSchema({
    lessonId: { type: String },
  }).validator(),
  run({ lessonId }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    Lessons.remove(lessonId);
  },
});

export const getLastLesson = new ValidatedMethod({
  name: 'Lessons.getLast',
  validate: null,
  run() {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    const now = new Date();
    const currentUser = Meteor.user();

    let lastLesson;

    if (!this.isSimulation) {
      lastLesson = Lessons.findOne({
        'classroom._id': { $in: getUserClassroomIds(currentUser) },
        dueAt: { $gte: now }
      }, { sort: { dueAt: 1 } });
    }

    return lastLesson;
  },
});
