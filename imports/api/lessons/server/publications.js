import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import Lessons from '../lessons.js';

Meteor.publish('Lessons.single', function(lessonId) {
  check(lessonId, String);

  const query = { _id: lessonId };
  const options = {};

  return Lessons.find(query, options);
});

Meteor.publish('Lessons.inList', function(limit, search) {
  check(search, Match.Maybe(String));
  check(limit, Number);

  const query = {};
  const options = {
    sort: { createdAt: -1 },
    limit,
  };

  if (search) {
    _.extend(query, {
      subject: { $regex: search, $options: 'i' },
    });
  }

  return Lessons.find(query, options);
});

Meteor.publish('Lessons.inTimetable', function(startDate, endDate, classroomIds) {
  check(startDate, Date);
  check(endDate, Date);
  check(classroomIds, [Match.Maybe(String)]);

  const query = {
    'classroom._id': { $in: classroomIds },
    dueAt: { $gt: startDate, $lt: endDate },
  };
  const options = {};

  return Lessons.find(query, options);
});
