import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Classrooms from '../classrooms/classrooms';
import Tasks from '../tasks/tasks';
import Lessons from '../lessons/lessons';

export const getDashboardCounts = new ValidatedMethod({
  name: 'Dashboard.counts',
  validate: null,
  run() {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    let usersCount = 0;
    let classroomsCount = 0;
    let tasksCount = 0;
    let lessonsCount = 0;

    if (!this.isSimulation) {
      usersCount = Meteor.users.find({ roles: 'student' }, { fields: { _id: 1 } }).count();
      classroomsCount = Classrooms.find({}, { fields: { _id: 1 } }).count();
      tasksCount = Tasks.find({}, { fields: { _id: 1 } }).count();
      lessonsCount = Lessons.find({}, { fields: { _id: 1 } }).count();
    }

    return { usersCount, classroomsCount, tasksCount, lessonsCount };
  },
});
