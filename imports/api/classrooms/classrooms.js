import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

const Classrooms = new Mongo.Collection('Classrooms');

Classrooms.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Classrooms.schema = new SimpleSchema({
  name: { type: String },
  studentCount: { type: Number, defaultValue: 0 },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();
      }
    },
  },
});

Classrooms.attachSchema(Classrooms.schema);

export default Classrooms;

