import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import Classrooms from './classrooms';

export const submitClassroom = new ValidatedMethod({
  name: 'Classrooms.submit',
  validate: new SimpleSchema({
    classroomId: { type: String, optional: true },
    name: { type: String },
  }).validator(),
  run({ classroomId, name }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    if (classroomId) {
      Classrooms.update(classroomId, {
        $set: { name },
      });

      if (!this.isSimulation) {
        Meteor.defer(() => {
          Meteor.users.update({ 'profile.classrooms._id': classroomId }, {
            $set: { 'profile.classrooms.$.name': name }
          }, { multi: true });
        });
      }
    } else {
      Classrooms.insert({ name });
    }
  },
});

export const deleteClassroom = new ValidatedMethod({
  name: 'Classrooms.delete',
  validate: new SimpleSchema({
    classroomId: { type: String },
  }).validator(),
  run({ classroomId }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    Classrooms.remove(classroomId);
  },
});

