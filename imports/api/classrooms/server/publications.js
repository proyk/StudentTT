import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import Classrooms from '../classrooms.js';

Meteor.publish('Classrooms.single', function(classroomId) {
  check(classroomId, String);

  const query = { _id: classroomId };
  const options = {};

  return Classrooms.find(query, options);
});

Meteor.publish('Classrooms.inList', function(limit, search) {
  check(search, Match.Maybe(String));
  check(limit, Number);

  const query = {};
  const options = {
    sort: { createdAt: -1 },
    limit,
  };

  if (search) {
    _.extend(query, {
      name: { $regex: search, $options: 'i' },
    });
  }

  return Classrooms.find(query, options);
});

Meteor.publish('Classrooms.inSelect', function() {
  const query = {};
  const options = {
    sort: { createdAt: -1 },
    fields: { name: 1 },
  };

  return Classrooms.find(query, options);
});
