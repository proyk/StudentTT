import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

const Notifications = new Mongo.Collection('Notifications');

Notifications.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Notifications.schema = new SimpleSchema({
  userId: { type: String },
  text: { type: String },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();
      }
    },
  },
});

Notifications.attachSchema(Notifications.schema);

export default Notifications;

