import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import Notifications from './notifications';

export const addNotification = new ValidatedMethod({
  name: 'Notifications.new',
  validate: new SimpleSchema({
    userId: { type: String },
    text: { type: String },
  }).validator(),
  run({ userId, text }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    Notifications.insert({ userId, text });
  },
});

export const deleteNotification = new ValidatedMethod({
  name: 'Notifications.delete',
  validate: new SimpleSchema({
    notificationId: { type: String },
  }).validator(),
  run({ notificationId }) {
    if (!this.userId) {
      throw new Meteor.Error(402, 'Must be login!');
    }

    Notifications.remove(notificationId);
  },
});

