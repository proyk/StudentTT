import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import Notifications from '../notifications';

Meteor.publish('Notifications.inList', function(userId, limit) {
  check(userId, String);
  check(limit, Number);

  const query = { userId };
  const options = {
    sort: { createdAt: -1 },
    limit,
  };

  return Notifications.find(query, options);
});

Meteor.publish('Notifications.onDashboard', function() {
  if (!this.userId) {
    return this.ready();
  }

  const query = { userId: this.userId };
  const options = {
    sort: { createdAt: -1 },
    limit: 1,
  };

  return Notifications.find(query, options);
});

