import '../../api/users/server/publications';
import '../../api/users/server/onCreateUser';
import '../../api/users/methods';

import '../../api/classrooms/server/publications';
import '../../api/classrooms/methods';

import '../../api/tasks/server/publications';
import '../../api/tasks/methods';

import '../../api/lessons/server/publications';
import '../../api/lessons/methods';

import '../../api/dashboard/methods';

import '../../api/notifications/server/publications';
import '../../api/notifications/methods';


