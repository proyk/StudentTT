import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Accounts } from 'meteor/accounts-base';

Meteor.startup(() => {
  const userExists = Meteor.users.findOne();

  if (!userExists) {
    const userId = Accounts.createUser({
      email: 'admin@app.com',
      password: '123456',
      profile: { name: 'admin' }
    });

    Roles.addUsersToRoles(userId, ['admin']);
  }
});

