import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { Meteor } from 'meteor/meteor';

import Layout from '../../ui/components/layout';
import AdminLayout from '../../ui/containers/admin_layout';

import SignIn from '../../ui/components/sign_in';
import SignUp from '../../ui/components/sign_up';
import App from '../../ui/containers/app';
import Dashboard from '../../ui/containers/dashboard';

import { NotFound } from '../../ui/pages/not_found/not_found';

import ClassroomsList from '../../ui/components/classrooms/list';
import NewClassroom from '../../ui/components/classrooms/submit';
import UpdateClassroom from '../../ui/containers/classrooms/submit';

import TasksList from '../../ui/components/tasks/list';
import NewTask from '../../ui/components/tasks/submit';
import UpdateTask from '../../ui/containers/tasks/submit';

import LessonsList from '../../ui/components/lessons/list';
import NewLesson from '../../ui/components/lessons/submit';
import UpdateLesson from '../../ui/containers/lessons/submit';

import UsersList from '../../ui/components/users/list';
import NewUser from '../../ui/components/users/new_user';
import EditUser from '../../ui/containers/users/edit_user';

import StudentTasks from '../../ui/containers/student/tasks';
import Timetable from '../../ui/components/student/timetable';
import Notifications from '../../ui/containers/notifications';

const authenticate = (nextState, replace) => {
  if (!Meteor.loggingIn() && !Meteor.userId()) {
    replace({
      pathname: '/sign-in',
      state: { nextPathname: nextState.location.pathname },
    });
  }
};

const redirectDashboard = (nextState, replace) => {
  replace({
    pathname: '/dashboard',
    state: { nextPathname: nextState.location.pathname },
  });
};

export const Routes = () => (
  <Router history={ browserHistory }>
    <Route path="/" component={ Layout }>
      <IndexRoute onEnter={ redirectDashboard } />

      <Route path="/sign-in" component={ SignIn }/>
      <Route path="/sign-up" component={ SignUp }/>

      <Route component={ App } onEnter={ authenticate }>
        <Route path="/dashboard" component={ Dashboard } />

        <Route component={ AdminLayout }>
          <Route path="/users/list" component={ UsersList } />
          <Route path="/users/new" component={ NewUser } />
          <Route path="/users/:userId/edit" component={ EditUser } />
          <Route path="/classrooms/list" component={ ClassroomsList } />
          <Route path="/classrooms/new" component={ NewClassroom } />
          <Route path="/classrooms/:classroomId/update" component={ UpdateClassroom } />
          <Route path="/tasks/list" component={ TasksList } />
          <Route path="/tasks/new" component={ NewTask } />
          <Route path="/tasks/:taskId/update" component={ UpdateTask } />
          <Route path="/lessons/list" component={ LessonsList } />
          <Route path="/lessons/new" component={ NewLesson } />
          <Route path="/lessons/:lessonId/update" component={ UpdateLesson } />
        </Route>

        <Route path="/tasks" component={ StudentTasks } />
        <Route path="/timetable" component={ Timetable } />
        <Route path="/users/:userId/notifications" component={ Notifications } />
      </Route>

      <Route path="*" component={ NotFound } />
    </Route>
  </Router>
);
