import moment from 'moment';

const occurrenceDay = ({ dueAt=new Date() }) => {
  return moment(dueAt).startOf('day').format();
};

const groupToDay = (group, day) => {
  return {
    day: day,
    tasks: group
  }
};

export const groupTasksByDay = ({ tasks = [] }) =>
  _.chain(tasks)
  .groupBy(occurrenceDay)
  .map(groupToDay)
  .sortBy('day')
  .reverse()
  .value();
