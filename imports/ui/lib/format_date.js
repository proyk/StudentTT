import moment from 'moment';

export const formatDate = (date, dateFormat) => {
  if (!(date instanceof Date)) {
    return '-';
  }

  return moment(date).format(dateFormat || 'DD/MM/YYYY, HH:mm:ss');
};

export const getDateRange = ({ date = new Date(), range = 'days' }) => ({
  startDate: moment(date).startOf(range).toDate(),
  endDate: moment(date).endOf(range).toDate(),
});

export const getDatesArrayWithWeeksCount = ({ weeks = 0 }) => {
  const startOfWeek = moment().startOf('week');
  const endOfWeek = moment().add(weeks, 'w').endOf('week');

  const days = [];
  let day = startOfWeek;

  while (day <= endOfWeek) {
    days.push(day.format('YYYY-MM-DD'));
    day = day.clone().add(1, 'd');
  }

  return days;
};

export const findDateIndex = ({ date, dateArray }) => {
  const formattedDate = formatDate(date, 'YYYY-MM-DD');
  return dateArray.indexOf(formattedDate);
}
