export const sortLessonsByDate = (arr) => {
  return _.sortBy(arr, 'dueAt').reverse();
};
