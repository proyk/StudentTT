export const getEmail = (user) => {
  return user && user.emails && user.emails.length ? user.emails[0].address : '';
};

export const getClassrooms = (user) => {
  return user && user.profile && user.profile.classrooms ? user.profile.classrooms : [];
};

export const getUserClassroomIds = (user) => {
  const classrooms = getClassrooms(user);
  return _.pluck(classrooms, '_id');
};
