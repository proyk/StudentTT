import React from 'react';
import './not_found.css';
import { Link } from 'react-router';

export const NotFound = () => (
  <div className="error-page">
    <h2 className="headline text-yellow"> 404</h2>

    <div className="error-content">
      <h3><i className="fa fa-warning text-yellow"/> Oops! Page not found.</h3>

      <p>
        We could not find the page you were looking for. Meanwhile, you may
        <Link to="/dashboard"> return to dashboard</Link>
      </p>

    </div>
  </div>
);
