import React from 'react';
import { Loading } from '../loading';
import { formatDate } from '../../lib/format_date';
import TasksItem from './tasks_item.jsx';

const renderTasks = (tasks) => tasks.map((task) => (
  <TasksItem task={task}/>
));

const renderTaskDays = (groupTasks) => {
  if (groupTasks.length === 0) {
    return (
      <li className="time-label">
        <span className="bg-yellow">
          <i className="fa fa-smile-o"></i> No tasks yet.
        </span>
      </li>
    );
  }

  return groupTasks.map((group) => {
    const day = (
      <li className="time-label">
        <span className="bg-red">
          {formatDate(new Date(group.day), 'D MMMM YYYY')}
        </span>
      </li>
    );

    return [ day, renderTasks(group.tasks) ];
  });
};

const StudentTasks = ({ loading, tasks }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      <section className="content-header">
        <h1>
          Tasks
        </h1>
      </section>

      <section className="content">

        <div className="row">
          <div className="col-md-12">

            <ul className="timeline">
              {renderTaskDays(tasks)}
            </ul>

          </div>
        </div>

      </section>
    </div>
  );
};

export default StudentTasks;
