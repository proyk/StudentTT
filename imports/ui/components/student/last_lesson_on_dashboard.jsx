import React, { Component } from 'react';
import { formatDate } from '../../lib/format_date';
import { getLastLesson } from '../../../api/lessons/methods';
import Alert from 'react-s-alert';
import { Loading } from '../loading';

class LastLesson extends Component {
  constructor() {
    super();

    this.state = {
      lastLesson: null,
      loading: true,
    };
  }

  componentDidMount() {
    this.getLastLesson();
  }

  getLastLesson() {
    getLastLesson.call(null, (error, lastLesson) => {
      this.setState({ loading: false });
      if (!error) {
        this.setState({ lastLesson });
      } else {
        Alert.warning(error.reason);
      }
    });
  }

  renderLesson() {
    const { lastLesson } = this.state;

    if (!lastLesson) {
      return (
        <div>
          <span className="info-box-number">No lesson next time</span>
        </div>
      );
    }

    return (
      <div>
        <span className="info-box-text">Next Lesson Due Date</span>
        <span className="info-box-number">{formatDate(lastLesson.dueAt)}</span>
      </div>
    );
  }

  render() {
    return (
      <div className="info-box">
        <span className="info-box-icon bg-red">
          <i className="fa fa-bell-o"></i>
        </span>
        <div className="info-box-content">
          {this.renderLesson()}
        </div>
      </div>
    );
  }
};

export default LastLesson;

