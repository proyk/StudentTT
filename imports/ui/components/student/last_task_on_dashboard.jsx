import React, { Component } from 'react';
import { formatDate } from '../../lib/format_date';
import { getLastTask } from '../../../api/tasks/methods';
import Alert from 'react-s-alert';
import { Loading } from '../loading';

class LastTask extends Component {
  constructor() {
    super();

    this.state = {
      lastTask: null,
      loading: true,
    };
  }

  componentDidMount() {
    this.getLastTask();
  }

  getLastTask() {
    getLastTask.call(null, (error, lastTask) => {
      this.setState({ loading: false });
      if (!error) {
        this.setState({ lastTask });
      } else {
        Alert.warning(error.reason);
      }
    });
  }

  renderTask() {
    const { lastTask } = this.state;

    if (!lastTask) {
      return (
        <div>
          <span className="info-box-number">No task next time</span>
        </div>
      );
    }

    return (
      <div>
        <span className="info-box-text">Next Task Due Date</span>
        <span className="info-box-number">{formatDate(lastTask.dueAt)}</span>
      </div>
    );
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    }

    return (
      <div className="info-box">
        <span className="info-box-icon bg-aqua">
          <i className="fa fa-file-text-o"></i>
        </span>
        <div className="info-box-content">
          {this.renderTask()}
        </div>
      </div>
    );
  }
};

export default LastTask;

