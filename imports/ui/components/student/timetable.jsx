import React, { Component } from 'react';
import TimetableToolbar from './timetable_toolbar';
import TimetableLessons from '../../containers/student/timetable_lessons';

class Timetable extends Component {
  constructor() {
    super();

    this.state = {
      date: new Date(),
    };

    this.onChangeDate = this.onChangeDate.bind(this);
  }

  onChangeDate(date) {
    this.setState({ date });
  }

  render() {
    return (
      <div>
        <section className="content-header">
          <h1>
            Timetable
          </h1>
        </section>

        <section className="content">
          <div className="box box-primary">
            <div className="box-body">
              <TimetableToolbar onChangeDate={this.onChangeDate} />
              <TimetableLessons date={this.state.date} />
            </div>
          </div>
        </section>
      </div>
    );
  }
};

export default Timetable;
