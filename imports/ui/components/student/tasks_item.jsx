import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { formatDate } from '../../lib/format_date';
import { Editor, EditorState, convertFromRaw } from 'draft-js';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';
import { completeTask } from '../../../api/tasks/methods';

class TasksItem extends Component {
  constructor() {
    super();
  }

  completeTask(taskId) {
    Progress.show();
    completeTask.call({ taskId }, (error) => {
      Progress.hide();
      if (error) {
        Alert.error(error.reason);
      } else {
        Alert.success('Task completed!');
      }
    });
  }

  renderCompletedButton(task) {
    const isCompleted = !!task && !!task.users && _.contains(task.users, Meteor.userId());

    return isCompleted ? (
      <button className="btn btn-success btn-xs">
        <i className="fa fa-check-square-o"></i> Completed
      </button>
    ): (
      <button className="btn btn-warning btn-xs" onClick={() => this.completeTask(task._id)}>
        <i className="fa fa-square-o"></i> Complete
      </button>
    );
  }

  renderDetails(rawValue) {
    const contentState = convertFromRaw(rawValue);
    const editorState = EditorState.createWithContent(contentState);

    return <Editor editorState={editorState} readOnly />
  };

  render() {
    const { task } = this.props;

    return (
      <li>
        <i className="fa fa-leaf bg-blue"></i>

        <div className="timeline-item">
          <span className="time">
            <i className="fa fa-clock-o"></i> {formatDate(task.dueAt, 'HH:mm')}
          </span>

          <h3 className="timeline-header"><strong>{task.subject}</strong></h3>

          <div className="timeline-body">
            {this.renderDetails(task.details)}
          </div>
          <div className="timeline-footer">
            {this.renderCompletedButton(task)}
          </div>
        </div>

      </li>
    );
  }
};

export default TasksItem;
