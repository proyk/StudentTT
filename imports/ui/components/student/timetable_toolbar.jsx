import React, { Component } from 'react';
import { getDatesArrayWithWeeksCount, findDateIndex, formatDate } from '../../lib/format_date';

class TimetableToolbar extends Component {
  constructor() {
    super();

    const dateArray = getDatesArrayWithWeeksCount({ weeks: 0 });
    this.todayIndex = findDateIndex({ date: new Date(), dateArray });

    this.state = {
      dateIndex: this.todayIndex,
      dateArray,
    };

    this.changeWeeksCount = this.changeWeeksCount.bind(this);
  }

  changeWeeksCount(e) {
    const weeks = e.target.value;

    const dateArray = getDatesArrayWithWeeksCount({ weeks });
    this.setState({ dateArray });
  }

  isDisableNextButton() {
    const { dateIndex, dateArray } = this.state;

    const isLastIndex = (dateArray.length - 1) === dateIndex;
    return isLastIndex;
  }

  isDisablePrevButton() {
    const { dateIndex } = this.state;
    return dateIndex === 0;
  }

  setToday() {
    const { dateArray } = this.state;

    this.setState({ dateIndex: this.todayIndex });
    this.props.onChangeDate(new Date(dateArray[this.todayIndex]));
  }

  changeNavigation(num) {
    const { dateIndex, dateArray } = this.state;

    const newIndex = dateIndex + num;

    this.setState({
      dateIndex: newIndex,
    }, this.disableButtons);

    this.props.onChangeDate(new Date(dateArray[newIndex]));
  }

  render() {
    const { dateIndex, dateArray } = this.state;

    return (
      <div className="row">

        <div className="col-lg-3 col-sm-12">
          <div className="form-group">
            <select className="form-control" onChange={this.changeWeeksCount}>
              <option value={0}>One week</option>
              <option value={1}>Two week</option>
              <option value={2}>Three week</option>
              <option value={3}>Four week</option>
            </select>
          </div>
        </div>

        <div className="col-lg-6 col-sm-12">
          <div style={{ textAlign: 'center' }}>
            <h5><strong>{formatDate(new Date(dateArray[dateIndex]), 'dddd, D MMM')}</strong></h5>
          </div>
        </div>

        <div className="col-lg-3 col-sm-12">
          <div className="btn-group pull-right">
            <button type="button" className="btn btn-danger" onClick={() => this.setToday()}>Today</button>
            <button type="button" className="btn btn-primary" onClick={() => this.changeNavigation(-1)} disabled={this.isDisablePrevButton()}>
              <i className="fa fa-chevron-left"></i>
            </button>
            <button type="button" className="btn btn-primary" onClick={() => this.changeNavigation(1)} disabled={this.isDisableNextButton()}>
              <i className="fa fa-chevron-right"></i>
            </button>
          </div>
        </div>

      </div>
    );
  }
};

export default TimetableToolbar;
