import React from 'react';
import { Loading } from '../loading';
import { formatDate } from '../../lib/format_date';
import { sortLessonsByDate } from '../../lib/lessons';

const getItemColor = (i) => {
  const colors = [ 'info', 'warning', 'success', 'danger' ];
  const index = i % 4;
  return colors[index];
};

const renderLessons = (lessons) => {
  if (!lessons.length) {
    return (
      <div className={`callout callout-warning`}>
        <p>No lessons</p>
      </div>
    );
  }

  const sortedLessons = sortLessonsByDate(lessons);

  return sortedLessons.map((lesson, i) => (
    <div key={lesson._id} className={`callout callout-${getItemColor(i)}`}>
      <span className="time pull-right">
        <i className="fa fa-clock-o"></i> {formatDate(lesson.dueAt, 'HH:mm')}
      </span>
      <p><i className="fa fa-pencil"></i> {lesson.subject}</p>
      <p><i className="fa fa-user-circle-o"></i> {lesson.teacher}</p>
      <p><i className="fa fa-university"></i> {lesson.room}</p>
    </div>
  ));
}

const TimetableLessons = ({ loading, lessons }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      {renderLessons(lessons)}
    </div>
  );
};

export default TimetableLessons;
