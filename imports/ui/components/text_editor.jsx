import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertFromRaw, convertToRaw } from 'draft-js';

class TextEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editorState: EditorState.createEmpty(),
    };

    this.onEditorStateChange = this.onEditorStateChange.bind(this);
  }

  componentDidMount() {
    this.setDefault();
  }

  setDefault() {
    const { rawValue } = this.props;

    if (rawValue) {
      const contentState = convertFromRaw(rawValue);
      const editorState = EditorState.createWithContent(contentState);

      this.setState({
        editorState
      });
    }
  }

  onEditorStateChange(editorState) {
    this.setState({
      editorState,
    });

    const contentState = editorState.getCurrentContent();
    const rawValue = convertToRaw(contentState);

    this.props.onChange(rawValue);
  }

  render() {

    return (
      <Editor
         wrapperClassName="wrapper-class"
         editorClassName="editor-class"
         toolbarClassName="toolbar-class"
         editorState={this.state.editorState}
         onEditorStateChange={this.onEditorStateChange} />
    );
  }
};

export default TextEditor;
