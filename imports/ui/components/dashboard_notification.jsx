import React from 'react';

const DashboardNotification = ({ lastNotification }) => {
  if (!lastNotification) {
    return <noscript />;
  }

  const { text } = lastNotification;

  return (
    <div className="callout callout-warning">
      <h4 className="box-title"><i className="fa fa-warning"></i> Notice</h4>
      <p>{text}</p>
    </div>
  );
};

export default DashboardNotification;
