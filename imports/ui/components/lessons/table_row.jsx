import React from 'react';
import { Link } from 'react-router';
import Confirm from 'react-confirm-bootstrap';
import { deleteLesson } from '../../../api/lessons/methods.js';
import Progress from 'react-progress-2';
import Alert from 'react-s-alert';
import { formatDate } from '../../lib/format_date.js';

const handleDeleteLesson = (lessonId) => {
  Progress.show();
  deleteLesson.call({ lessonId }, (error) => {
    Progress.hide();
    if (error) {
      Alert.error(error.reason);
    } else {
      Alert.success('Deleted');
    }
  });
};

const Row = ({ lesson }) => (
  <tr role="row" className="odd">
    <td>{lesson.subject}</td>
    <td>{lesson.teacher}</td>
    <td>{lesson.room}</td>
    <td>{lesson.classroom.name}</td>
    <td>{formatDate(lesson.dueAt)}</td>
    <td>
      <Link to={`/lessons/${lesson._id}/update`} className="btn btn-primary">
        <i className="fa fa-edit"></i>
      </Link>
      <Confirm
        onConfirm={() => handleDeleteLesson(lesson._id)}
        body="Are you sure you want to delete this?"
        confirmText="Confirm Delete"
        title="Deleting lesson">
        <button className="btn btn-danger">
          <i className="fa fa-trash"></i>
        </button>
      </Confirm>
    </td>
  </tr>
);

export default Row;

