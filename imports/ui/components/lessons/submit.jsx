import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import Alert from 'react-s-alert';
import { submitLesson } from '../../../api/lessons/methods';
import Progress from 'react-progress-2';
import { Loading } from '../loading';
import SelectClassrooms from '../../containers/select_classrooms.js';
import DateTime from 'react-datetime';

class SubmitLessons extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      classroom: null,
      dueAt: new Date(),
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setDefault(nextProps);
  }

  setDefault(props) {
    const { params, lesson } = props;

    if (params.lessonId && lesson) {
      this.setState({
        classroom: lesson.classroom,
        dueAt: lesson.dueAt,
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    const { lessonId } = this.props.params;

    const subject = this.refs.subject.value;
    const teacher = this.refs.teacher.value;
    const room = this.refs.room.value;
    const classroom = this.state.classroom;
    const dueAt = this.state.dueAt;

    if (!subject) {
      return Alert.warning('Need a subject here.');
    }

    if (!teacher) {
      return Alert.warning('Need a teacher here.');
    }

    if (!room) {
      return Alert.warning('Need a room here.');
    }

    if (!classroom) {
      return Alert.warning('Need classroom here');
    }

    Progress.show();
    submitLesson.call({ lessonId, subject, teacher, room, classroom, dueAt }, (error) => {
      Progress.hide();
      if (error) {
        Alert.error(error.reason);
      } else {
        Alert.success('Successful');
        browserHistory.push('/lessons/list');
      }
    });
  }

  renderHeader() {
    const { lessonId } = this.props.params;
    if (lessonId) {
      return (<h3 className="box-title">Edit Lesson</h3>);
    } else {
      return (<h3 className="box-title">New Lesson</h3>);
    }
  }

  render() {
    const { lesson, loading } = this.props;

    if (loading) {
      return <Loading />;
    }

    return (
      <section className="content">
        <div className="row">
          <div className="col-md-6">
            <div className="box box-info">
              <div className="box-header with-border">
                {this.renderHeader()}
              </div>

              <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <div className="box-body">
                  <div className="form-group">
                    <label className="col-sm-2 control-label">Subject</label>

                    <div className="col-sm-10">
                      <input type="text" ref="subject" className="form-control"  defaultValue={lesson.subject} />
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="col-sm-2 control-label">Teacher</label>

                    <div className="col-sm-10">
                      <input type="text" ref="teacher" className="form-control" defaultValue={lesson.teacher} />
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="col-sm-2 control-label">Room</label>

                    <div className="col-sm-10">
                      <input type="text" ref="room" className="form-control" defaultValue={lesson.room} />
                    </div>
                  </div>

                  <SelectClassrooms defaultClassrooms={lesson.classroom} onSelect={(classroom) => this.setState({ classroom })} />

                  <div className="form-group">
                    <label className="col-sm-2 control-label">Due At</label>
                    <div className="col-sm-10">
                      <DateTime
                         defaultValue={lesson.dueAt}
                         onChange={(val) => this.setState({ dueAt: val.toDate() })} />
                    </div>
                  </div>
                </div>

                <div className="box-footer">
                  <Link to="/lessons/list" className="btn btn-default">
                    Cancel
                  </Link>
                  <button type="submit" className="btn btn-success pull-right">Submit</button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
};

SubmitLessons.defaultProps = {
  loading: false,
  lesson: {
    subject: '',
    teacher: '',
    room: '',
    classroom: {},
    dueAt: new Date(),
  }
};

export default SubmitLessons;
