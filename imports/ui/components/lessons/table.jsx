import React from 'react';
import TableRow from './table_row';
import { Loading } from '../loading';

const renderRows = (lessons) => {
  return lessons.map((lesson) => <TableRow key={lesson._id} lesson={lesson}/>)
};

const Table = ({ lessons, loading }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <div className="row">
      <div className="col-sm-12">
        <table className="table table-bordered table-striped dataTable" role="grid">
          <thead>
            <tr role="row">
              <th>Subject</th>
              <th>Teacher</th>
              <th>Room</th>
              <th>Classroom</th>
              <th>Due At</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {renderRows(lessons)}
          </tbody>
        </table>
      </div>
    </div>
  );
}
export default Table;
