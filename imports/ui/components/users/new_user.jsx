import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';
import { newUser } from '../../../api/users/methods';
import SelectClassrooms from '../../containers/select_classrooms';

class NewUser extends Component {
  constructor() {
    super();
    this.state = {
      classrooms: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    const name = this.refs.username.value;
    const email = this.refs.email.value;
    const password = this.refs.password.value;
    const classrooms = this.state.classrooms;

    if (!email) {
      return Alert.warning('Need an email address here.');
    }

    if (!name) {
      return Alert.warning('Need a name here.');
    }

    if (!password) {
      return Alert.warning('Need a password here.');
    }

    if (classrooms.length === 0) {
      return Alert.warning('Need a classroom here.');
    }

    Progress.show();
    newUser.call({ name, email, password, classrooms }, (error) => {
      Progress.hide();
      if (error) {
        Alert.warning(error.reason);
      } else {
        browserHistory.push('/users/list');
      }
    });
  }

  render() {
    return (
      <section className="content">
        <div className="row">
          <div className="col-md-6">
            <div className="box box-info">
              <div className="box-header with-border">
                <h3 className="box-title">New User</h3>
              </div>

              <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <div className="box-body">
                  <div className="form-group">
                    <label className="col-sm-2 control-label">Email</label>

                    <div className="col-sm-10">
                      <input type="email" ref="email" className="form-control" placeholder="email" />
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="col-sm-2 control-label">Name</label>

                    <div className="col-sm-10">
                      <input type="text" ref="username" className="form-control" placeholder="Name" />
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="col-sm-2 control-label">Password</label>

                    <div className="col-sm-10">
                      <input type="password" ref="password" className="form-control" placeholder="Password" />
                    </div>
                  </div>

                  <SelectClassrooms multi={true} onSelect={(classrooms) => this.setState({ classrooms })}/>

                </div>

                <div className="box-footer">
                  <Link to="/users/list" className="btn btn-default">
                    Cancel
                  </Link>
                  <button type="submit" className="btn btn-success pull-right">Submit</button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
};

export default NewUser;
