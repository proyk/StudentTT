import React from 'react';
import { Loading } from '../loading';
import { getEmail } from '../../lib/users';
import EditUserEmail from './edit_user_email';
import EditUserPassword from './edit_user_password';
import EditUserName from './edit_user_name';
import EditUserClassrooms from './edit_user_classrooms';

const EditUser = ({ user, loading }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      <section className="content-header">
        <h1>
          Edit Student
        </h1>
      </section>
      <section className="content">
        <div className="row">
          <EditUserEmail userId={user._id} email={getEmail(user)} />
          <EditUserPassword userId={user._id} />
          <EditUserName userId={user._id} name={user.profile.name} />
          <EditUserClassrooms userId={user._id} classrooms={user.profile.classrooms} />
        </div>
      </section>
    </div>
  );
};

EditUser.defaultProps = {
  user: { name: '' },
  loading: false,
};

export default EditUser;
