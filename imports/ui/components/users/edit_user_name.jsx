import React, { Component } from 'react';
import { changeUserName } from '../../../api/users/methods';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';

class EditUserName extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    const { userId } = this.props;
    const name = this.refs.userName.value;

    if (!name) {
      return Alert.warning('Need a name here.');
    }

    Progress.show();
    changeUserName.call({ userId, name }, (error) => {
      Progress.hide();
      if (error) {
        Alert.warning(error.reason);
      } else {
        Alert.success('User name changed.');
      }
    });
  }

  render() {
    const { name } = this.props;
    return (
      <div className="col-md-6">
        <div className="box box-info">
          <div className="box-header with-border">
            Edit User Name
          </div>

          <form className="form-horizontal" onSubmit={this.handleSubmit}>
            <div className="box-body">

              <div className="form-group">
                <label className="col-sm-2 control-label">Name</label>

                <div className="col-sm-10">
                  <input type="text" ref="userName" className="form-control" defaultValue={name} />
                </div>
              </div>

            </div>

            <div className="box-footer">
              <button type="submit" className="btn btn-success pull-right">Submit</button>
            </div>

          </form>
        </div>
      </div>
    );
  }
};

export default EditUserName;

