import React, { Component } from 'react';
import { changeUserClassrooms } from '../../../api/users/methods';
import SelectClassrooms from '../../containers/select_classrooms';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';

class EditUserClassrooms extends Component {
  constructor() {
    super();
    this.state = {
      classrooms: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  };

  handleSubmit(e) {
    e.preventDefault();

    const { userId } = this.props;
    const classrooms = this.state.classrooms;

    if (classrooms.length === 0) {
      return Alert.warning('Need a classroom here.');
    }

    Progress.show();
    changeUserClassrooms.call({ userId, classrooms }, (error) => {
      Progress.hide();
      if (error) {
        Alert.warning(error.reason);
      } else {
        Alert.success('User classrooms changed.');
      }
    });
  }

  render() {
    const { classrooms } = this.props;
    return (
      <div className="col-md-6">
        <div className="box box-info">
          <div className="box-header with-border">
            Change Classrooms
          </div>

          <form className="form-horizontal" onSubmit={this.handleSubmit}>
            <div className="box-body">
              <SelectClassrooms
                 multi={true}
                 onSelect={(classrooms) => this.setState({ classrooms })}
                 defaultClassrooms={classrooms} />
            </div>

            <div className="box-footer">
              <button type="submit" className="btn btn-success pull-right">Submit</button>
            </div>

          </form>
        </div>
      </div>
    );
  }
};

export default EditUserClassrooms;
