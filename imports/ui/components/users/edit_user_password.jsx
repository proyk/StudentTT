import React, { Component } from 'react';
import { setUserPassword } from '../../../api/users/methods';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';

class EditUserPassword extends Component {
  constructor() {
    super();
    this.setPassword = this.setPassword.bind(this);
  }

  setPassword(e) {
    e.preventDefault();

    const { userId } = this.props;
    const newPassword = this.refs.password.value;

    if (!newPassword) {
      return Alert.warning('Need a new password here.');
    }

    Progress.show();
    setUserPassword.call({ userId, newPassword }, (error) => {
      Progress.hide();
      if (error) {
        Alert.warning(error.reason);
      } else {
        this.refs.password.value = '';
        Alert.success('User password changed.');
      }
    });
  }

  render() {
    return (
      <div className="col-md-6">
        <div className="box box-info">
          <div className="box-header with-border">
            Change Password
          </div>

          <form className="form-horizontal" onSubmit={this.setPassword}>
            <div className="box-body">

              <div className="form-group">
                <label className="col-sm-2 control-label">New Password</label>

                <div className="col-sm-10">
                  <input type="password" ref="password" className="form-control" placeholder="Password" />
                </div>
              </div>
            </div>

            <div className="box-footer">
              <button type="submit" className="btn btn-success pull-right">Submit</button>
            </div>

          </form>
        </div>
      </div>
    );
  }
};

export default EditUserPassword;
