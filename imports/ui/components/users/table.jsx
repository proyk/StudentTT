import React from 'react';
import TableRow from './table_row';
import { Loading } from '../loading';

const renderRows = (users) => {
  return users.map((user) => <TableRow key={user._id} user={user}/>)
};

const Table = ({ users, loading }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <div className="row">
      <div className="col-sm-12">
        <table className="table table-bordered table-striped dataTable" role="grid">
          <thead>
            <tr role="row">
              <th>Name</th>
              <th>Email</th>
              <th>Classrooms</th>
              <th>CreatedAt</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {renderRows(users)}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Table;
