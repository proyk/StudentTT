import React from 'react';
import { Link } from 'react-router';
import Confirm from 'react-confirm-bootstrap';
import { deleteUser } from '../../../api/users/methods.js';
import Progress from 'react-progress-2';
import Alert from 'react-s-alert';
import { formatDate } from '../../lib/format_date.js';
import { getEmail, getClassrooms } from '../../lib/users';

const renderUserClassrooms = (user) => {
  return (
    <select className="form-control">
      {
        getClassrooms(user).map((classroom) => {
          return (
            <option key={classroom._id}>{classroom.name}</option>
          );
        })
      }
    </select>
  );
};

const handleDelete = ({ userId, classrooms=[] }) => {
  Progress.show();

  deleteUser.call({ userId, classrooms }, (error) => {
    Progress.hide();
    if (error) {
      Alert.error(error.reason);
    } else {
      Alert.success('Deleted');
    }
  });
};

const Row = ({ user }) => (
  <tr role="row" className="odd">
    <td>{user.profile.name}</td>
    <td>{getEmail(user)}</td>
    <td>{renderUserClassrooms(user)}</td>
    <td>{formatDate(user.createdAt, 'DD/MM/YYYY')}</td>
    <td>
      <Link to={`/users/${user._id}/edit`} className="btn btn-primary">
        <i className="fa fa-edit"></i>
      </Link>
      <Link to={`/users/${user._id}/notifications`} className="btn btn-warning">
        <i className="fa fa-bell-o"></i>
      </Link>
      <Confirm
        onConfirm={
           () => handleDelete({
             userId: user._id,
             classrooms: user.profile.classrooms
           })
        }
        body="Are you sure you want to delete this?"
        confirmText="Confirm Delete"
        title="Deleting user">
        <button className="btn btn-danger">
          <i className="fa fa-trash"></i>
        </button>
      </Confirm>
    </td>
  </tr>
);

export default Row;

