import React, { Component } from 'react';
import { changeUserEmail } from '../../../api/users/methods';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';

class EditUserEmail extends Component {
  constructor() {
    super();
    this.changeEmail = this.changeEmail.bind(this);
  }

  changeEmail(e) {
    e.preventDefault();

    const { userId } = this.props;
    const email = this.refs.mail.value;

    Progress.show();
    changeUserEmail.call({ userId, email }, (error) => {
      Progress.hide();
      if (error) {
        Alert.warning(error.reason);
      } else {
        Alert.success('E-mail address changed.');
      }
    });
  }

  render() {
    const { email } = this.props

    return (
      <div className="col-md-6">
        <div className="box box-info">
          <div className="box-header with-border">
            Change Email
          </div>

          <form className="form-horizontal" onSubmit={this.changeEmail}>
            <div className="box-body">
              <div className="form-group">
                <label className="col-sm-2 control-label">Email</label>

                <div className="col-sm-10">
                  <input type="text" ref="mail" className="form-control" defaultValue={email} />
                </div>
              </div>
            </div>

            <div className="box-footer">
              <button type="submit" className="btn btn-success pull-right">Submit</button>
            </div>
          </form>

        </div>
      </div>
    );
  }
};

export default EditUserEmail;
