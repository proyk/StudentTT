import React from 'react';

class FilterBar extends React.Component {
  constructor() {
    super();
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChangeLimit = this.handleChangeLimit.bind(this);
  }

  handleSearch(e) {
    const searchQuery = this.refs.searchQuery.value.trim();

    if (e.key === 'Enter') {
      this.props.onClickSearch(searchQuery);
    }
  }

  handleChangeLimit(e) {
    const limit = parseInt(e.target.value, 10);
    this.props.onChangeLimit(limit);
  }

  render() {
    return (
      <div className="row">
        <div className="col-sm-6">
          <div className="dataTables_length">
            <label>
              Show
              <select
                className="form-control input-sm"
                onChange={this.handleChangeLimit}
                defaultValue={10} size="1"
              >
                <option key={0} value="10">10</option>
                <option key={1} value="25">25</option>
                <option key={2} value="50">50</option>
                <option key={3} value="100">100</option>
              </select>
              entries
            </label>
          </div>
        </div>

        <div className="col-sm-6">
          <div className="dataTables_filter pull-right">
            <label>
              Search: <input type="search" ref="searchQuery" onKeyPress={this.handleSearch} className="form-control input-sm" placeholder="" />
            </label>
          </div>
        </div>
      </div>
    );
  }
};

FilterBar.propTypes = {
  onChangeLimit: React.PropTypes.func,
  onClickSearch: React.PropTypes.func,
};

export default FilterBar;
