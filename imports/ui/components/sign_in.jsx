import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import { Meteor } from 'meteor/meteor';
import Alert from 'react-s-alert';

export default class SignIn extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const email = this.refs.email.value;
    const password = this.refs.password.value;

    if (!email) {
      return Alert.warning('Need an email address here.');
    }

    if (!password) {
      return Alert.warning('Need a password here.');
    }

    Meteor.loginWithPassword(email, password, (error) => {
      if (error) {
        Alert.warning(error.reason);
      } else {
        browserHistory.push('/');
      }
    });
  }

  render() {
    return (
      <div className="login-box">
	<div className="login-logo">
          <Link to={'/dashboard'}><b>App</b></Link>
	</div>

	<div className="login-box-body">
	  <p className="login-box-msg">Sign in to start your session</p>

	  <form onSubmit={this.handleSubmit}>
	    <div className="form-group has-feedback">
	      <input type="email" className="form-control" placeholder="Email" ref="email"/>
	      <span className="glyphicon glyphicon-envelope form-control-feedback" />
	    </div>
	    
	    <div className="form-group has-feedback">
	      <input type="password" className="form-control" placeholder="Password" ref="password"/>
	      <span className="glyphicon glyphicon-lock form-control-feedback" />
	    </div>

	    <div className="row">
	      <div className="pull-right col-xs-4">
		<button type="submit" className="btn btn-primary btn-block btn-flat">Sign In</button>
	      </div>
	    </div>
	  </form>

          <Link to={'/sign-up'}>Register</Link>

	</div>
      </div>
    );
  }
}
