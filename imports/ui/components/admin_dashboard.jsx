import React, { Component } from 'react';
import { Link } from 'react-router';
import { getDashboardCounts } from '../../api/dashboard/methods';

class AdminDashboard extends Component {
  constructor() {
    super();

    this.state = {
      counts: {
        usersCount: 0,
        classroomsCount: 0,
        tasksCount: 0,
        lessonsCount: 0,
      },
    };
  }

  setDashboardCounts() {
    getDashboardCounts.call(null, (error, counts) => {
      if (!error) {
        this.setState({ counts });
      }
    });
  }

  componentDidMount() {
    this.setDashboardCounts();
  }

  render() {
    const { usersCount, classroomsCount, tasksCount, lessonsCount } = this.state.counts;

    return (
      <section className="content">
        <div className="row">
          <div className="col-lg-3 col-xs-6">
            <div className="small-box bg-aqua">
              <div className="inner">
                <h3>{usersCount}</h3>
                <p>Students</p>
              </div>
              <div className="icon">
                <i className="fa fa-users"></i>
              </div>
              <Link to={ '/users/list' } className="small-box-footer">
                More info <i className="fa fa-arrow-circle-right"></i>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-xs-6">
            <div className="small-box bg-green">
              <div className="inner">
                <h3>{classroomsCount}</h3>
                <p>Classrooms</p>
              </div>
              <div className="icon">
                <i className="fa fa-television"></i>
              </div>
              <Link to={ '/classrooms/list' } className="small-box-footer">
                More info <i className="fa fa-arrow-circle-right"></i>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-xs-6">
            <div className="small-box bg-yellow">
              <div className="inner">
                <h3>{tasksCount}</h3>
                <p>Tasks</p>
              </div>
              <div className="icon">
                <i className="fa fa-tasks"></i>
              </div>
              <Link to={ '/tasks/list' } className="small-box-footer">
                More info <i className="fa fa-arrow-circle-right"></i>
              </Link>
            </div>
          </div>

          <div className="col-lg-3 col-xs-6">
            <div className="small-box bg-red">
              <div className="inner">
                <h3>{lessonsCount}</h3>
                <p>Lessons</p>
              </div>
              <div className="icon">
                <i className="fa fa-graduation-cap"></i>
              </div>
              <Link to={ '/lessons/list' } className="small-box-footer">
                More info <i className="fa fa-arrow-circle-right"></i>
              </Link>
            </div>
          </div>

        </div>
      </section>
    );
  }
};

export default AdminDashboard;
