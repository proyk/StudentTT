import React, { Component } from 'react';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';
import { deleteNotification } from '../../api/notifications/methods';
import Confirm from 'react-confirm-bootstrap';

class EditNotification extends Component {
  constructor() {
    super();
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete() {
    const { notificationId } = this.props;

    Progress.show();
    deleteNotification.call({ notificationId }, (error) => {
      Progress.hide();
      if (error) {
        Alert.warning(error.reason);
      } else {
        Alert.success('Notification deleted!');
      }
    });
  }

  render() {
    const { isAdmin } = this.props;

    if (!isAdmin) {
      return <noscript />;
    }

    return (
      <div>
        <Confirm
          onConfirm={this.handleDelete}
          body="Are you sure you want to delete this?"
          confirmText="Confirm Delete"
          title="Deleting notification">
          <button className="btn btn-sm btn-danger">
            Delete
          </button>
        </Confirm>
      </div>
    );
  }
};

export default EditNotification;


