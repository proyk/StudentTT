import React from 'react';
import { Link } from 'react-router';

const StudentMenu = () => (
  <ul className="sidebar-menu">
    <li className="header">MAIN NAVIGATION</li>
    <li>
      <Link to={'/dashboard'} >
        <i className="fa fa-dashboard" /> <span>Dashboard</span>
      </Link>
    </li>
    <li>
      <Link to={'/timetable'} >
        <i className="fa fa-calendar" /> <span>Timetable</span>
      </Link>
    </li>
    <li>
      <Link to={'/tasks'} >
        <i className="fa fa-tasks" /> <span>Tasks</span>
      </Link>
    </li>
  </ul>
);

export default StudentMenu;
