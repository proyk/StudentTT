import React from 'react';
import { Roles } from 'meteor/alanning:roles';
import AdminMenu from './sidebar-admin-menu';
import StudentMenu from './sidebar-student-menu';

const SideBarMenu = ({ user }) => {
  const isAdmin = Roles.userIsInRole(user, 'admin');
  return isAdmin ? <AdminMenu /> : <StudentMenu />;
}

export default SideBarMenu;
