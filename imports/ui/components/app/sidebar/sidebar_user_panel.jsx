import React, { Component } from 'react';

export default class SideBarUserPanel extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const displayName = this.props.userName;
    
    return (
      <div className="user-panel">
        <div className="pull-left image">
          <img src="/img/avatar.png" className="user-image" alt="User Image"/>
        </div>
        <div className="pull-left info">
          <p>{ displayName }</p>
          <i className="fa fa-circle text-success"/> Online
        </div>
      </div>
    )
  }
}
