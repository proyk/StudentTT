import React, { Component, PropTypes } from 'react';
import SideBarUserPanel from './sidebar_user_panel';
import SideBarMenu from './sidebar_menu';

export default class SideBar extends Component {
  constructor(props) {
    super(props);
  }

  userDisplayName() {
    const currentUser = this.props.user;

    if (currentUser) {
      return currentUser.profile.name;
    }

    return 'username';
  };

  render() {

    return (
      <aside className="main-sidebar">
        <section className="sidebar">
	  <SideBarUserPanel userName={ this.userDisplayName() } />
	  <SideBarMenu user={this.props.user} />
        </section>
      </aside>
    );
  }
}
