import React from 'react';
import { Link } from 'react-router';

const AdminMenu = () => (
  <ul className="sidebar-menu">
    <li className="header">MAIN NAVIGATION</li>
    <li>
      <Link to={'/dashboard'} >
        <i className="fa fa-dashboard" /> <span>Dashboard</span>
      </Link>
    </li>
    <li>
      <Link to={ '/users/list' }><i className="fa fa-users"/> <span>Students</span></Link>
    </li>
    <li>
      <Link to={ '/classrooms/list' }><i className="fa fa-television"/> <span>Classrooms</span></Link>
    </li>
    <li>
      <Link to={ '/tasks/list' }><i className="fa fa-tasks"/> <span>Tasks</span></Link>
    </li>
    <li>
      <Link to={ '/lessons/list' }><i className="fa fa-graduation-cap"/> <span>Lessons</span></Link>
    </li>
  </ul>
);

export default AdminMenu;
