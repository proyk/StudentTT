import React, { PropTypes } from 'react';

import SideBar from './sidebar/sidebar.jsx';
import AppHeader from './app_header.jsx';
import AppFooter from './app_footer.jsx';

const App = ({ children, currentUser }) => {
  const contentMinHeight = {
    minHeight: ((window.innerHeight - 101) + 'px')
  };

  return (
    <div className="wrapper">
      <AppHeader user={ currentUser } />
      <SideBar user={ currentUser } />

      <div className="content-wrapper" style={ contentMinHeight }>
        { children }
      </div>

      <AppFooter />
      <div className="control-sidebar-bg"></div>
    </div>
  );
};

App.propTypes = {
  children: React.PropTypes.node,
  currentUser: PropTypes.object,
};

export default App;
