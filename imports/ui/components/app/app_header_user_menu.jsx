import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { browserHistory, Link } from 'react-router';

export default class AppHeaderUserMenu extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  userDisplayName() {
    const currentUser = this.props.user;

    if (currentUser) {
      return currentUser.profile.name;
    }

    return 'username';
  };

  logout() {
    Meteor.logout(() => {
      browserHistory.push('/sign-in');
    });
  }

  render() {

    return (
      <li className="dropdown user user-menu">
        <a href="#" className="dropdown-toggle" data-toggle="dropdown">
          <img src="/img/avatar.png" className="user-image" alt="User Image"/>
          <span className="hidden-xs">{ this.userDisplayName() }</span>
        </a>

        <ul className="dropdown-menu">

          <li className="user-header">
            <img src="/img/avatar.png" className="img-circle" alt="User Image"/>
            <p>{ this.userDisplayName() }</p>
          </li>

          <li className="user-footer">
            <div className="pull-left">
              <Link to="/profile" className="btn btn-default btn-flat">Profile</Link>
            </div>
            <div className="pull-right">
              <a href="#" className="btn btn-default btn-flat" onClick={ this.logout }>Sign out</a>
            </div>
          </li>

        </ul>
      </li>
    );
  }
}
