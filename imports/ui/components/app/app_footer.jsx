import React, { Component } from 'react';

export default class AppFooter extends Component {

  render() {
    return (
      <footer className="main-footer">
	<div className="pull-right hidden-xs">
	  <b>Version</b> 1.0.0
	</div>
	<strong>Copyright &copy; 2017-2018 </strong> All rights reserved.
      </footer>
    );
  }
}
