import React, { Component } from 'react';
import { Link } from 'react-router';
import AppHeaderUserMenu from './app_header_user_menu.jsx';

export default class AppHeader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { user } = this.props;
    return (
      <header className="main-header">

        <Link to="/dashboard" className="logo">
          <span className="logo-mini"><b>A</b></span>
          <span className="logo-lg"><b>App</b></span>
        </Link>

        <nav className="navbar navbar-static-top">
          <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span className="sr-only">Toggle navigation</span>
          </a>

          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <AppHeaderUserMenu user={user} />
            </ul>
          </div>
        </nav>

      </header>
    );
  }
}
