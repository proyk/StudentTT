import React from 'react';
import DashboardNotification from '../containers/dashboard_notification';
import LastTaskOnDashboard from './student/last_task_on_dashboard';
import LastLessonOnDashboard from './student/last_lesson_on_dashboard';

const StudentDashboard = () => (
  <section className="content">
    <div className="row">

      <div className="col-lg-12">
        <DashboardNotification />
      </div>

      <div className="col-lg-6 col-xs-12">
        <LastTaskOnDashboard />
      </div>

      <div className="col-lg-6 col-xs-12">
        <LastLessonOnDashboard />
      </div>

    </div>
  </section>
);

export default StudentDashboard;
