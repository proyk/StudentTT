import React from 'react';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';

const Layout = ({ children }) => (
  <div>
    <Alert stack={{limit: 3}} offset={80} effect="slide" position="top-right" />
    <Progress.Component />
    { children }
  </div>
);

Layout.propTypes = {
  children: React.PropTypes.node,
};

export default Layout;
