import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import Alert from 'react-s-alert';

export default class SignUp extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const name = this.refs.username.value;
    const email = this.refs.email.value;
    const password = this.refs.password.value;
    const reTypePass = this.refs.reTypePass.value;

    if (!name) {
      return Alert.warning('Need a name here.');
    }

    if (!email) {
      return Alert.warning('Need an email address here.');
    }

    if (!password) {
      return Alert.warning('Need a password here.');
    }

    if (password !== reTypePass) {
      return Alert.warning('Passwords do not match');
    }

    Accounts.createUser({
      email, password, profile: { name },
    }, (error) => {
      if (error) {
        Alert.warning(error.reason);
      } else {
        Meteor.loginWithPassword(email, password);
        browserHistory.push('/dashboard');
      }
    })
  }

  render() {
    return (
      <div className="login-box">
        <div className="register-logo">
          <Link to={'/dashboard'}><b>App</b></Link>
        </div>

        <div className="register-box-body">
          <p className="login-box-msg">Register</p>

          <form onSubmit={ this.handleSubmit }>
            <div className="form-group has-feedback">
              <input type="text" ref="username" className="form-control" placeholder="Name" />
              <span className="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div className="form-group has-feedback">
              <input type="email" ref="email" className="form-control" placeholder="Email" />
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div className="form-group has-feedback">
              <input type="password" ref="password" className="form-control" placeholder="Password" />
              <span className="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div className="form-group has-feedback">
              <input type="password" ref="reTypePass" className="form-control" placeholder="Retype password" />
              <span className="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            <div className="row">
              <div className="col-xs-4">
                <button type="submit" className="btn btn-primary btn-block btn-flat">Register</button>
              </div>
            </div>
          </form>

          <br/>
          <Link to={'/sign-in'}>
            I already have an account
          </Link>

        </div>
      </div>
    );
  }
}
