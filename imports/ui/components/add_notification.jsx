import React, { Component } from 'react';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';
import { addNotification } from '../../api/notifications/methods';

class AddNotification extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    const text = this.refs.message.value;

    if (!text.trim()) {
      return Alert.warning('Need a text here.');
    }

    const { userId } = this.props;

    Progress.show();
    addNotification.call({ text, userId }, (error) => {
      Progress.hide();
      if (error) {
        Alert.warning(error.reason);
      } else {
        this.refs.message.value = '';
        Alert.success('Notification added!');
      }
    });
  }

  render() {
    const { isAdmin } = this.props;

    if (!isAdmin) {
      return <noscript />;
    }

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="input-group">
            <input type="text" ref="message" placeholder="Type Message ..." className="form-control" />
            <span className="input-group-btn">
              <button type="submit" className="btn btn-success btn-flat">Add</button>
            </span>
          </div>
        </form>
        <br />
      </div>
    );
  }
};

export default AddNotification;
