import React from 'react';
import { Loading } from './loading';
import AdminDashboard from './admin_dashboard';
import StudentDashboard from './student_dashboard';

const Dashboard = ({ isAdmin, loading }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <div>
      <section className="content-header">
        <h1>Dashboard</h1>
      </section>
      {
        isAdmin ? <AdminDashboard /> : <StudentDashboard />
      }
    </div>
  );
};

export default Dashboard;
