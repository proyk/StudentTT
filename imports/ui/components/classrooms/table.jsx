import React from 'react';
import TableRow from './table_row';
import { Loading } from '../loading';

const renderRows = (classrooms) => {
  return classrooms.map((classroom) => <TableRow key={classroom._id} classroom={classroom}/>)
};

const Table = ({ classrooms, loading }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <div className="row">
      <div className="col-sm-12">
        <table className="table table-bordered table-striped dataTable" role="grid">
          <thead>
            <tr role="row">
              <th>Name</th>
              <th>Student Count</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {renderRows(classrooms)}
          </tbody>
        </table>
      </div>
    </div>
  );
}
export default Table;
