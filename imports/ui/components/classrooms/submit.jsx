import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import Alert from 'react-s-alert';
import { submitClassroom } from '../../../api/classrooms/methods';
import Progress from 'react-progress-2';
import { Loading } from '../loading';

class SubmitClassrooms extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    const { classroomId } = this.props.params;
    const name = this.refs.classroomName.value;

    const existsInput = name.trim();

    if (existsInput) {
      Progress.show();
      submitClassroom.call({ classroomId, name }, (error) => {
        Progress.hide();
        if (error) {
          Alert.error(error.reason);
        } else {
          Alert.success('Successful');
          browserHistory.push('/classrooms/list');
        }
      });
    }
  }

  renderHeader() {
    const { classroomId } = this.props.params;
    if (classroomId) {
      return (<h3 className="box-title">Edit Classroom</h3>);
    } else {
      return (<h3 className="box-title">New Classroom</h3>);
    }
  }

  render() {
    const { classroom, loading } = this.props;

    if (loading) {
      return <Loading />;
    }

    return (
      <section className="content">
        <div className="row">
          <div className="col-md-6">
            <div className="box box-info">
              <div className="box-header with-border">
                {this.renderHeader()}
              </div>

              <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <div className="box-body">
                  <div className="form-group">
                    <label className="col-sm-2 control-label">Name</label>

                    <div className="col-sm-10">
                      <input type="text" ref="classroomName" className="form-control" placeholder="Classroom Name" defaultValue={classroom.name} />
                    </div>
                  </div>
                </div>

                <div className="box-footer">
                  <Link to="/classrooms/list" className="btn btn-default">
                    Cancel
                  </Link>
                  <button type="submit" className="btn btn-success pull-right">Submit</button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
};

SubmitClassrooms.defaultProps = {
  classroom: { name: '' },
  loading: false,
};

export default SubmitClassrooms;
