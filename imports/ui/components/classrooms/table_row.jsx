import React from 'react';
import { Link } from 'react-router';
import Confirm from 'react-confirm-bootstrap';
import { deleteClassroom } from '../../../api/classrooms/methods.js';
import Progress from 'react-progress-2';
import Alert from 'react-s-alert';

const handleDeleteClassroom = (classroomId) => {
  Progress.show();
  deleteClassroom.call({ classroomId }, (error) => {
    Progress.hide();
    if (error) {
      Alert.error(error.reason);
    } else {
      Alert.success('Deleted');
    }
  });
};

const Row = ({ classroom }) => (
  <tr role="row" className="odd">
    <td>{classroom.name}</td>
    <td>{classroom.studentCount}</td>
    <td>
      <Link to={`/classrooms/${classroom._id}/update`} className="btn btn-primary">
        <i className="fa fa-edit"></i>
      </Link>
      <Confirm
        onConfirm={() => handleDeleteClassroom(classroom._id)}
        body="Are you sure you want to delete this?"
        confirmText="Confirm Delete"
        title="Deleting classroom">
        <button className="btn btn-danger">
          <i className="fa fa-trash"></i>
        </button>
      </Confirm>
    </td>
  </tr>
);

export default Row;

