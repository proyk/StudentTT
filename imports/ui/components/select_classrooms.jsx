import React, { Component } from 'react';
import Select from 'react-select';

class SelectClassrooms extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: props.defaultClassrooms || [],
    };

    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  handleSelectChange(selected) {
    const { onSelect } = this.props;

    this.setState({ selected });
    onSelect(selected);
  }

  render() {
    const { classrooms, loading, multi } = this.props;

    return (
      <div className="form-group">
        <label className="col-sm-2 control-label">
          {multi ? 'Classrooms' : 'Classroom'}
        </label>

        <div className="col-sm-10">
          <Select
             multi={multi}
             isLoading={loading}
             valueKey="_id"
             labelKey="name"
             value={this.state.selected}
             options={classrooms}
             onChange={this.handleSelectChange}
             />
        </div>
      </div>
    );
  }
};

SelectClassrooms.defaultProps = {
  multi: false,
};

export default SelectClassrooms;
