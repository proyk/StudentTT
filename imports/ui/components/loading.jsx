import React from 'react';

export const Loading = () => (
  <section className="content">
    <div className="row">
      <div className="col-xs-12">
        <div style={{ textAlign: 'center', 'fontSize': 50 }}>
          <i className="fa fa-refresh fa-spin"></i>
        </div>
      </div>
    </div>
  </section>
);
