import React, { Component } from 'react';
import AddNotification from './add_notification';
import EditNotification from './edit_notification';
import { formatDate } from '../lib/format_date';

class Notifications extends Component {
  constructor() {
    super();
  }

  renderNotifications() {
    const { notifications, isAdmin } = this.props;

    return notifications.map((item) => (
      <div key={item._id} className="callout callout-info">
        <span className="time pull-right">
          <i className="fa fa-calendar"></i> {formatDate(item.createdAt)}
        </span>
        <p>
          {item.text}
        </p>

        <EditNotification notificationId={item._id} isAdmin={isAdmin} />
      </div>
    ));
  }

  render() {
    const { params, isAdmin } = this.props;
    const { userId } = params;

    return (
      <section className="content">
        <div className="row">
          <div className="col-xs-12">
            <div className="box">

              <div className="box-header">
                <h3 className="box-title">Notifications</h3>
              </div>

              <div className="box-body">
                <AddNotification userId={userId} isAdmin={isAdmin} />
                {this.renderNotifications()}
              </div>

            </div>
          </div>
        </div>
      </section>
    );
  }
};

export default Notifications;
