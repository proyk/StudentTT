import React, { PropTypes } from 'react';
import { Loading } from './loading';

const AdminLayout = ({ children, isAdmin, loading }) => {
  if (loading) {
    return <Loading />;
  }

  if (isAdmin) {
    return (
      <div>
        { children }
      </div>
    );
  }

  return (
    <div className="row">
      <div className="col-xs-12">
        <div className="box">
          <div className="box-body">
            <div className="alert alert-danger alert-dismissible">
              <h4><i className="icon fa fa-ban"></i> Alert!</h4>
              You don't have permissions to access this page.
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

AdminLayout.propTypes = {
  children: PropTypes.node,
  isAdmin: PropTypes.bool,
};

export default AdminLayout;
