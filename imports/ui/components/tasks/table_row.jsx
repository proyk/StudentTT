import React from 'react';
import { Link } from 'react-router';
import Confirm from 'react-confirm-bootstrap';
import { deleteTask } from '../../../api/tasks/methods.js';
import Progress from 'react-progress-2';
import Alert from 'react-s-alert';
import { formatDate } from '../../lib/format_date.js';

const handleDeleteTask = (taskId) => {
  Progress.show();
  deleteTask.call({ taskId }, (error) => {
    Progress.hide();
    if (error) {
      Alert.error(error.reason);
    } else {
      Alert.success('Deleted');
    }
  });
};

const Row = ({ task }) => (
  <tr role="row" className="odd">
    <td>{task.subject}</td>
    <td>{task.classroom.name}</td>
    <td>{formatDate(task.createdAt)}</td>
    <td>{formatDate(task.dueAt)}</td>
    <td>
      <Link to={`/tasks/${task._id}/update`} className="btn btn-primary">
        <i className="fa fa-edit"></i>
      </Link>
      <Confirm
        onConfirm={() => handleDeleteTask(task._id)}
        body="Are you sure you want to delete this?"
        confirmText="Confirm Delete"
        title="Deleting task">
        <button className="btn btn-danger">
          <i className="fa fa-trash"></i>
        </button>
      </Confirm>
    </td>
  </tr>
);

export default Row;

