import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import Alert from 'react-s-alert';
import Progress from 'react-progress-2';
import { Loading } from '../loading';
import { submitTask } from '../../../api/tasks/methods';
import SelectClassrooms from '../../containers/select_classrooms.js';
import DateTime from 'react-datetime';
import TextEditor from '../text_editor';

class SubmitTasks extends Component {
  constructor(props) {
    super(props);

    this.state = {
      details: null,
      classroom: null,
      dueAt: new Date(),
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setDefault(nextProps);
  }

  setDefault(props) {
    const { params, task } = props;

    if (params.taskId && task) {
      this.setState({
        details: task.details,
        classroom: task.classroom,
        dueAt: task.dueAt,
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    const { taskId } = this.props.params;
    const subject = this.refs.subject.value;
    const details = this.state.details;
    const classroom = this.state.classroom;
    const dueAt = this.state.dueAt;

    if (!subject) {
      return Alert.warning('Need a subject here.');
    }

    if (!details) {
      return Alert.warning('Need details here.');
    }

    if (!classroom) {
      return Alert.warning('Need classroom here');
    }

    Progress.show();
    submitTask.call({ taskId, subject, details, classroom, dueAt }, (error) => {
      Progress.hide();
      if (error) {
        Alert.error(error.reason);
      } else {
        Alert.success('Successful');
        browserHistory.push('/tasks/list');
      }
    });

  }

  renderHeader() {
    const { taskId } = this.props.params;
    if (taskId) {
      return (<h3 className="box-title">Edit Task</h3>);
    } else {
      return (<h3 className="box-title">New Task</h3>);
    }
  }

  render() {
    const { task, loading } = this.props;

    if (loading) {
      return <Loading />;
    }

    return (
      <section className="content">
        <div className="row">
          <div className="col-md-12">
            <div className="box box-info">
              <div className="box-header with-border">
                {this.renderHeader()}
              </div>

              <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <div className="box-body">

                  <div className="form-group">
                    <label className="col-sm-2 control-label">Subject</label>

                    <div className="col-sm-10">
                      <input type="text" ref="subject" className="form-control" placeholder="Task's subject" defaultValue={task.subject} />
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="col-sm-2 control-label">Details</label>
                    <div className="col-sm-10">
                      <TextEditor rawValue={task.details} onChange={(details) => this.setState({ details })} />
                    </div>
                  </div>

                  <SelectClassrooms defaultClassrooms={task.classroom} onSelect={(classroom) => this.setState({ classroom })} />

                  <div className="form-group">
                    <label className="col-sm-2 control-label">Due At</label>
                    <div className="col-sm-10">
                      <DateTime
                         defaultValue={task.dueAt}
                         onChange={(val) => this.setState({ dueAt: val.toDate() })} />
                    </div>
                  </div>

                </div>

                <div className="box-footer">
                  <Link to="/tasks/list" className="btn btn-default">
                    Cancel
                  </Link>
                  <button type="submit" className="btn btn-success pull-right">Submit</button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
};

SubmitTasks.defaultProps = {
  loading: false,
  task: {
    subject: '',
    details: '',
    classroom: {},
    dueAt: new Date(),
  }
};

export default SubmitTasks;
