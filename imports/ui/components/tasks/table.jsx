import React from 'react';
import TableRow from './table_row';
import { Loading } from '../loading';

const renderRows = (tasks) => {
  return tasks.map((task) => <TableRow key={task._id} task={task}/>)
};

const Table = ({ tasks, loading }) => {
  if (loading) {
    return <Loading />;
  }

  return (
    <div className="row">
      <div className="col-sm-12">
        <table className="table table-bordered table-striped dataTable" role="grid">
          <thead>
            <tr role="row">
              <th>Subject</th>
              <th>Classroom</th>
              <th>Created At</th>
              <th>Due At</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {renderRows(tasks)}
          </tbody>
        </table>
      </div>
    </div>
  );
}
export default Table;
