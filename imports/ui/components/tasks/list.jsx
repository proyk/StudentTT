import React, { Component } from 'react';
import { Link } from 'react-router';
import FilterBar from '../filter_bar';
import Table from '../../containers/tasks/table';

class List extends Component {
  constructor() {
    super();

    this.state = {
      limit: 10,
      search: null,
    };
  }

  render() {
    return (
      <section className="content">
        <div className="row">
          <div className="col-xs-12">
            <div className="box">

              <div className="box-header">
                <h3 className="box-title">Tasks</h3>
                <Link to="/tasks/new" className="btn btn-success pull-right">
                  <i className="fa fa-plus"></i> Add Task
                </Link>
              </div>

              <div className="box-body">
                <div className="dataTables_wrapper form-inline dt-bootstrap">
                  <FilterBar
                    onChangeLimit={(limit) => this.setState({ limit })}
                    onClickSearch={(search) => this.setState({ search })} />
                  <Table limit={this.state.limit} search={this.state.search} />
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default List;
