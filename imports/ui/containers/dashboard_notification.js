import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Notifications from '../../api/notifications/notifications';
import DashboardNotification from '../components/dashboard_notification';

export default createContainer(() => {
  const userId = Meteor.userId();

  const handle = Meteor.subscribe('Notifications.onDashboard');
  const loading = !handle.ready();

  const lastNotification = Notifications.findOne({ userId });

  return {
    loading,
    lastNotification,
  };
}, DashboardNotification);
