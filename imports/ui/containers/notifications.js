import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { createContainer } from 'meteor/react-meteor-data';
import Notifications from '../../api/notifications/notifications';
import NotificationsComp from '../components/notifications';

const NotificationsContainer = createContainer(({ params }) => {
  const { userId } = params;
  const limit = 10;

  const currentUser = Meteor.user();
  const isAdmin = currentUser && Roles.userIsInRole(currentUser, 'admin');

  const handle = Meteor.subscribe('Notifications.inList', userId, limit);
  const loading = !handle.ready();

  const notifications = Notifications.find({
    userId,
  }, { limit, sort: { createdAt: -1 } }).fetch();

  return {
    loading,
    notifications,
    isAdmin,
  };
}, NotificationsComp);

export default NotificationsContainer;
