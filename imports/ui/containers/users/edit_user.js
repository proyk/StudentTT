import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import EditUser from '../../components/users/edit_user';

const ListContainer = createContainer(({ params }) => {
  const { userId } = params;

  const handle = Meteor.subscribe('Users.single', userId);
  const loading = !handle.ready();

  const user = Meteor.users.findOne(userId);

  return {
    loading,
    user,
  };
}, EditUser);

export default ListContainer;
