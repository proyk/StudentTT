import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Table from '../../components/users/table';

const ListContainer = createContainer(({ limit, search }) => {
  const handle = Meteor.subscribe('Users.inList', limit, search);
  const loading = !handle.ready();

  const query = {
    roles: { $ne: 'admin' },
  };
  const options = {
    sort: { createdAt: -1 },
    limit,
  };

  const users = Meteor.users.find(query, options).fetch();

  return {
    loading,
    users,
  };
}, Table);

export default ListContainer;

