import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Lessons from '../../../api/lessons/lessons';
import Table from '../../components/lessons/table';

const ListContainer = createContainer(({ limit, search }) => {
  const lessonsHandle = Meteor.subscribe('Lessons.inList', limit, search);
  const loading = !lessonsHandle.ready();

  const lessons = Lessons.find({}, { limit, sort: { createdAt: -1 } }).fetch();

  return {
    loading,
    lessons,
  };
}, Table);

export default ListContainer;

