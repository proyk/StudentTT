import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Lessons from '../../../api/lessons/lessons';
import Submit from '../../components/lessons/submit';

const ListContainer = createContainer(({ params }) => {
  const { lessonId } = params;

  const lessonsHandle = Meteor.subscribe('Lessons.single', lessonId);
  const loading = !lessonsHandle.ready();

  const lesson = Lessons.findOne(lessonId);

  return {
    loading,
    lesson,
  };
}, Submit);

export default ListContainer;
