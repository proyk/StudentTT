import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Tasks from '../../../api/tasks/tasks';
import Submit from '../../components/tasks/submit';

const ListContainer = createContainer(({ params }) => {
  const { taskId } = params;

  const tasksHandle = Meteor.subscribe('Tasks.single', taskId);
  const loading = !tasksHandle.ready();

  const task = Tasks.findOne(taskId);

  return {
    loading,
    task,
  };
}, Submit);

export default ListContainer;
