import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Tasks from '../../../api/tasks/tasks';
import Table from '../../components/tasks/table';

const ListContainer = createContainer(({ limit, search }) => {
  const tasksHandle = Meteor.subscribe('Tasks.inList', limit, search);
  const loading = !tasksHandle.ready();

  const tasks = Tasks.find({}, { limit, sort: { createdAt: -1 } }).fetch();

  return {
    loading,
    tasks,
  };
}, Table);

export default ListContainer;

