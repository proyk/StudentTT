import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { getDateRange } from '../../lib/format_date';
import Lessons from '../../../api/lessons/lessons';
import TimetableLessons from '../../components/student/timetable_lessons';

const TimetableLessonsContainer = createContainer(({ date }) => {
  const currentUser = Meteor.user();
  const classrooms = currentUser && currentUser.profile
          && currentUser.profile.classrooms ? currentUser.profile.classrooms : [];

  const classroomIds = _.pluck(classrooms, '_id');
  const { startDate, endDate } = getDateRange({ date, range: 'day' });

  const lessonsHandle = Meteor.subscribe('Lessons.inTimetable', startDate, endDate, classroomIds);
  const loading = !lessonsHandle.ready();

  const query = {
    'classroom._id': { $in: classroomIds },
    dueAt: { $gt: startDate, $lt: endDate },
  };

  const lessons = Lessons.find(query).fetch();

  return {
    loading,
    lessons,
  };
}, TimetableLessons);

export default TimetableLessonsContainer;
