import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Tasks from '../../../api/tasks/tasks';
import StudentTasks from '../../components/student/tasks';
import { groupTasksByDay } from '../../lib/tasks';

const StudentTasksContainer = createContainer(() => {
  const currentUser = Meteor.user();
  const classrooms = currentUser && currentUser.profile
          && currentUser.profile.classrooms ? currentUser.profile.classrooms : [];

  const classroomIds = _.pluck(classrooms, '_id');
  const limit = 10;

  const tasksHandle = Meteor.subscribe('Tasks.forStudent', classroomIds, limit);
  const loading = !tasksHandle.ready();

  const tasks = Tasks.find({
    'classroom._id': { $in: classroomIds },
  }, { limit, sort: { createdAt: -1 } }).fetch();

  return {
    loading,
    tasks: groupTasksByDay({ tasks }),
  };
}, StudentTasks);

export default StudentTasksContainer;


