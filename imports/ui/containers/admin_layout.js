import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Roles } from 'meteor/alanning:roles';

import AdminLayout from '../components/admin_layout';

export default createContainer(() => {
  const currentUser = Meteor.user();

  const loading = !currentUser;
  const isAdmin = currentUser && Roles.userIsInRole(currentUser, 'admin');

  return {
    loading,
    isAdmin,
  };
}, AdminLayout);

