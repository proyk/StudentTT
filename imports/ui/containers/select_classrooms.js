import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Classrooms from '../../api/classrooms/classrooms';
import SelectClassrooms from '../components/select_classrooms';

const SelectClassroomsContainer = createContainer(() => {
  const classroomsHandle = Meteor.subscribe('Classrooms.inSelect');
  const loading = !classroomsHandle.ready();

  const classrooms = Classrooms.find({}, { sort: { createdAt: -1 } }).fetch();

  return {
    loading,
    classrooms,
  };
}, SelectClassrooms);

export default SelectClassroomsContainer;


