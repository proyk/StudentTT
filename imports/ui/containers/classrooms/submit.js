import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Classrooms from '../../../api/classrooms/classrooms';
import Submit from '../../components/classrooms/submit';

const ListContainer = createContainer(({ params }) => {
  const { classroomId } = params;

  const classroomsHandle = Meteor.subscribe('Classrooms.single', classroomId);
  const loading = !classroomsHandle.ready();

  const classroom = Classrooms.findOne(classroomId);

  return {
    loading,
    classroom,
  };
}, Submit);

export default ListContainer;
