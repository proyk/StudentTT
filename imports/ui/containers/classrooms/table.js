import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import Classrooms from '../../../api/classrooms/classrooms';
import Table from '../../components/classrooms/table';

const ListContainer = createContainer(({ limit, search }) => {
  const classroomsHandle = Meteor.subscribe('Classrooms.inList', limit, search);
  const loading = !classroomsHandle.ready();

  const classrooms = Classrooms.find({}, { limit, sort: { createdAt: -1 } }).fetch();

  return {
    loading,
    classrooms,
  };
}, Table);

export default ListContainer;

